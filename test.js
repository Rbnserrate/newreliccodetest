/* eslint no-unused-expressions: "off" */
import {join as joinPaths} from 'path'
import App from './src/app.js'
import ApplicationData from './src/applicationData.js'
import Host from './src/host.js'
import {importer} from './src/jsonHelper.js'
import {expect} from 'chai'

describe('ApplicationData tests', () => {
  /**
   * Creates and returns a list of three different Host objects
   * @returns {Host[]} - List of three different Host objects
   */
  function getThreeDifferentHostInstances () {
    return [new Host('host1'), new Host('host2'), new Host('host3')]
  }
  it('Instance attributes are set correctly through constructor.', done => {
    let name = 'name'
    let contributors = ['contributor1', 'contributor2']
    let version = 3
    let apdex = 98
    let applicationDataInstance = new ApplicationData({
      name: name,
      contributors: contributors,
      version: version,
      apdex: apdex
    })

    expect(applicationDataInstance.getName()).to.equal(name)
    expect(applicationDataInstance._contributors).to.have.members(contributors)
    expect(applicationDataInstance.getVersion()).to.equal(version)
    expect(applicationDataInstance.getApdex()).to.equal(apdex)
    done()
  })
  it('Application registers deployments in hosts correctly.', done => {
    let applicationDataInstance = new ApplicationData({})
    let hostList = getThreeDifferentHostInstances()
    expect(applicationDataInstance.getDeployments().length).to.equal(0)
    hostList.forEach(applicationDataInstance.registerDeployment.bind(applicationDataInstance))
    expect(hostList).to.have.members(applicationDataInstance.getDeployments())
    done()
  })
  it('Registering a deployment in a host that was already registered has no effect.', done => {
    let applicationDataInstance = new ApplicationData({})
    let hostList = getThreeDifferentHostInstances()
    expect(applicationDataInstance.getDeployments().length).to.equal(0)
    hostList.forEach(applicationDataInstance.registerDeployment.bind(applicationDataInstance))
    applicationDataInstance.registerDeployment(hostList[1])
    expect(hostList).to.have.members(applicationDataInstance.getDeployments())
    done()
  })
  it('Application removes deployments from hosts correctly.', done => {
    let applicationDataInstance = new ApplicationData({})
    let hostList = getThreeDifferentHostInstances()
    hostList.forEach(applicationDataInstance.registerDeployment.bind(applicationDataInstance))
    applicationDataInstance.removeDeployment(hostList[1])
    expect(applicationDataInstance.getDeployments()).to.have.members([hostList[0], hostList[2]])
    done()
  })
  it('Trying to remove a deployment that doesn`t exist has no effect.', done => {
    let applicationDataInstance = new ApplicationData({})
    let hostList = getThreeDifferentHostInstances()
    hostList.forEach(applicationDataInstance.registerDeployment.bind(applicationDataInstance))
    applicationDataInstance.removeDeployment(new Host({}))
    expect(hostList).to.have.members(applicationDataInstance.getDeployments())
    done()
  })
})

describe('Host tests', () => {
  /**
   * Creates and returns a list of three different application data objects
   * @returns {ApplicationData[]} - List of three different application data objects
   */
  function getThreeDifferentApplicationDataInstances () {
    return [
      new ApplicationData({
        name: 'name1',
        version: '1'
      }),
      new ApplicationData({
        name: 'name2',
        version: '1'
      }),
      new ApplicationData({
        name: 'name1',
        version: '2'
      })]
  }
  it('Instance attributes are set correctly through constructor.', done => {
    let URL = 'host.com'
    let hostInstance = new Host(URL)
    expect(hostInstance.getURL()).to.equal(URL)
    done()
  })
  it('Applications are registered correctly.', done => {
    let hostInstance = new Host()
    let appList = getThreeDifferentApplicationDataInstances()
    expect(hostInstance.getAllHostedApps().length).to.equal(0)
    appList.forEach(hostInstance.registerApp.bind(hostInstance))
    expect(appList).to.have.members(hostInstance.getAllHostedApps())
    done()
  })
  it('Applications are deregistered correctly.', done => {
    let hostInstance = new Host()
    let appList = getThreeDifferentApplicationDataInstances()
    appList.forEach(hostInstance.registerApp.bind(hostInstance))
    hostInstance.unregisterApp(appList[1])
    expect(hostInstance.getAllHostedApps()).to.have.members([appList[0], appList[2]])
    done()
  })
  it('Deregistering an app that`s not hosted has no effect.', done => {
    let hostInstance = new Host()
    let appList = getThreeDifferentApplicationDataInstances()
    appList.forEach(hostInstance.registerApp.bind(hostInstance))
    hostInstance.unregisterApp(new ApplicationData({}))
    expect(appList).to.have.members(hostInstance.getAllHostedApps())
    done()
  })
  it('Registering an app that`s already hosted has no effect.', done => {
    let hostInstance = new Host()
    let appList = getThreeDifferentApplicationDataInstances()
    appList.forEach(hostInstance.registerApp.bind(hostInstance))
    hostInstance.registerApp(appList[0])
    expect(appList).to.have.members(hostInstance.getAllHostedApps())
    done()
  })
  describe('Top apps list contains the 25 highest rated apps in order', done => {
    /**
     * Creates a list of ApplicationData with correlative apdex scores (0 to numApps)
     * randomly sorted.
     * @param {Number} numApps - Number of apps to include in the list
     * @returns {ApplicationDatap[]} - List of ApplicationData
     */
    function createShuffledApplicationDataArray (numApps) {
      return Array.apply(null, Array(numApps))
        .map((x, index) => {
          return {
            app: new ApplicationData({name: 'n', version: index, apdex: index}),
            sortValue: Math.random()
          }
        })
        .sort((a, b) => a.sortValue - b.sortValue)
        .map(x => x.app)
    }
    /**
     * "Expects" all the ApplicationData objects in listA to have the same value of "_apdex" as
     * the ApplicationData object in listB in the same index
     * @param {ApplicationData[]} listA - List of ApplicationData A
     * @param {ApplicationData[]} listB - List of ApplicationData B
     * @returns {void}
     */
    function expectTwoApplicationDataListsInTheSameOrder (listA, listB) {
      expect(listA.map(x => x.getApdex())).to.have.ordered.members(listB.map(x => x.getApdex()))
    }

    it('When we have less than 25 apps registered', done => {
      let hostInstance = new Host()
      let shuffledAppList = createShuffledApplicationDataArray(13)
      shuffledAppList.forEach(hostInstance.registerApp.bind(hostInstance))
      expectTwoApplicationDataListsInTheSameOrder(
        shuffledAppList.sort((a, b) => b.getApdex() - a.getApdex()),
        hostInstance.getTopApps()
      )
      done()
    })

    it('When we have more than 25 apps registered', done => {
      let hostInstance = new Host()
      let shuffledAppList = createShuffledApplicationDataArray(65)
      shuffledAppList.forEach(hostInstance.registerApp.bind(hostInstance))
      expectTwoApplicationDataListsInTheSameOrder(
        shuffledAppList.sort((a, b) => b.getApdex() - a.getApdex()).slice(0, 25),
        hostInstance.getTopApps()
      )
      done()
    })

    it('When registering apps with repeated apdex', done => {
      let hostInstance = new Host()
      let shuffledAppList = createShuffledApplicationDataArray(50)
      shuffledAppList.forEach(hostInstance.registerApp.bind(hostInstance))
      let newApplicationData = new ApplicationData({apdex:
          getRandomElementFromArray(hostInstance.getTopApps()).getApdex()})
      shuffledAppList.push(newApplicationData)
      hostInstance.registerApp(newApplicationData)
      expectTwoApplicationDataListsInTheSameOrder(
        shuffledAppList.sort((a, b) => b.getApdex() - a.getApdex()).slice(0, 25),
        hostInstance.getTopApps()
      )
      done()
    })

    it('After registering apps that are already registered and in the top apps list', done => {
      let hostInstance = new Host()
      let shuffledAppList = createShuffledApplicationDataArray(50)
      shuffledAppList.forEach(hostInstance.registerApp.bind(hostInstance))
      hostInstance.registerApp(getRandomElementFromArray(hostInstance.getTopApps()))
      expectTwoApplicationDataListsInTheSameOrder(
        shuffledAppList.sort((a, b) => b.getApdex() - a.getApdex()).slice(0, 25),
        hostInstance.getTopApps()
      )
      done()
    })

    it('After deregistering apps when we have less than 25 apps registered', done => {
      let hostInstance = new Host()
      let shuffledAppList = createShuffledApplicationDataArray(15)
      shuffledAppList.forEach(hostInstance.registerApp.bind(hostInstance))
      let appToDeRegister = getRandomElementFromArray(shuffledAppList)
      hostInstance.unregisterApp(appToDeRegister)
      expectTwoApplicationDataListsInTheSameOrder(
        shuffledAppList.filter(x => x !== appToDeRegister).sort((a, b) => b.getApdex() - a.getApdex()),
        hostInstance.getTopApps()
      )
      done()
    })

    it('After deregistering a top app when we have more than 25 apps registered', done => {
      let hostInstance = new Host()
      let shuffledAppList = createShuffledApplicationDataArray(50)
      shuffledAppList.forEach(hostInstance.registerApp.bind(hostInstance))
      let appToDeRegister = getRandomElementFromArray(hostInstance.getTopApps())
      hostInstance.unregisterApp(appToDeRegister)
      expectTwoApplicationDataListsInTheSameOrder(
        shuffledAppList.filter(x => x !== appToDeRegister).sort((a, b) => b.getApdex() - a.getApdex()).slice(0, 25),
        hostInstance.getTopApps()
      )
      done()
    })

    it('After deregistering NON top app when we have more than 25 apps registered', done => {
      let hostInstance = new Host()
      let shuffledAppList = createShuffledApplicationDataArray(50)
      shuffledAppList.forEach(hostInstance.registerApp.bind(hostInstance))
      let randomNotTopApp = getRandomElementFromArray(shuffledAppList.filter(x => !hostInstance.getTopApps().includes(x)))
      hostInstance.unregisterApp(randomNotTopApp)
      expectTwoApplicationDataListsInTheSameOrder(
        shuffledAppList.filter(x => x !== randomNotTopApp).sort((a, b) => b.getApdex() - a.getApdex()).slice(0, 25),
        hostInstance.getTopApps()
      )
      done()
    })
  })
})

describe('Main application tests', () => {
  let app = new App()
  let dataToImport
  before(() => importer(joinPaths(__dirname, '/data/host-app-data.json'))
    .then(data => {
      dataToImport = data
      app.importApplicationData(dataToImport)
    })
    .catch(e => console.log('Error importing data: ', e)))

  describe('Application data importing and processing', () => {
    it('An applicationData object is created for each unique pair name-version in the imported dataset', done => {
      let nameVersionPairs = dataToImport.map(x => x.name + x.version)
      let uniquePairs = nameVersionPairs.filter(
        (pair, index) => nameVersionPairs.indexOf(pair) === index)
      expect(uniquePairs).to.have.members(app._getAllApps().map(x => x.getName() + x.getVersion()))
      done()
    })

    it('The applicationData objects are properly indexed and setup with the information from the imported dataset', done => {
      let randomAppDetails = getRandomElementFromArray(dataToImport)
      let randomApplicationDataObject = app._appsByNameAndVersion[randomAppDetails.name][randomAppDetails.version]
      expect(randomApplicationDataObject.getName()).to.equal(randomAppDetails.name)
      expect(randomApplicationDataObject.getVersion()).to.equal(randomAppDetails.version)
      expect(randomApplicationDataObject.getApdex()).to.equal(randomAppDetails.apdex)
      expect(randomApplicationDataObject._contributors).to.have.members(randomAppDetails.contributors)
      expect(randomApplicationDataObject.getDeployments().map(x => x._url)).to.have.members(randomAppDetails.host)
      done()
    })

    it('ApplicationData objects hold references to Host objects instead of strings with the host names', done => {
      let randomAppDetails = getRandomElementFromArray(dataToImport)
      let randomApplicationDataObject = app._appsByNameAndVersion[randomAppDetails.name][randomAppDetails.version]
      randomApplicationDataObject.getDeployments().forEach(host => expect(host).to.be.an.instanceOf(Host))
      done()
    })

    it('For every different value inside the `host` property of the imported data, a Host object is created', done => {
      let differentHostValues = {}
      dataToImport.forEach(appDataObject => appDataObject.host.forEach(hostName => {
        if (!differentHostValues[hostName]) differentHostValues[hostName] = true
      }))
      expect(app._hostsByName).to.have.all.keys(differentHostValues)
      done()
    })

    it('ApplicationData objects hold references to the Host objects where they´re hosted', done => {
      let randomAppDetails = getRandomElementFromArray(dataToImport)
      let randomApplicationDataObject = app._appsByNameAndVersion[randomAppDetails.name][randomAppDetails.version]
      expect(randomAppDetails.host).to.have.members(randomApplicationDataObject.getDeployments().map(x => x.getURL()))
      done()
    })

    it('Host instances hold references to all applicationData objects that are hosted in them', done => {
      let randomHostName = getRandomElementFromArray(Object.keys(app._hostsByName))
      let HostInstance = app._hostsByName[randomHostName]
      let nameOfAppsHostedInThisHost = dataToImport.reduce((appsHosted, appToCheck) => {
        if (appToCheck.host.includes(randomHostName)) appsHosted.push(appToCheck.name)
        return appsHosted
      }, [])
      expect(nameOfAppsHostedInThisHost).to.have.members(HostInstance.getAllHostedApps().map(x => x.getName()))
      done()
    })
  })
  describe('Application retrieves the 25 top apps list by host sorted by apdex', () => {
    /**
     * "Expects" all the plain objects in listA to have the same value for "apdex" as
     * the ApplicationData object in listB in the same index for the property "_apdex"
     * @param {Object[]} listA - List of plain objects with app information A
     * @param {ApplicationData[]} listB - List of ApplicationData b
     * @returns {void}
     */
    function expectPlainObjectListAndApplicationDataListInTheSameOrder (listA, listB) {
      expect(listA.map(x => x.apdex)).to.have.ordered.members(listB.map(x => x.getApdex()))
    }

    it('Returns an empty array if the host doesn`t exist', done => {
      expect(app.getTopAppsByHost('non.existent.host').length).to.equal(0)
      done()
    })

    it('Returns the list correctly for an existing host', done => {
      let randomHostName = getRandomElementFromArray(Object.keys(app._hostsByName))
      // First of all, we find in the imported data, all applications that are hosted in this host
      let detailsForAppsHostedInThisHost = dataToImport.reduce((appsHosted, appToCheck) => {
        if (appToCheck.host.includes(randomHostName)) appsHosted.push(appToCheck)
        return appsHosted
      }, [])
      expectPlainObjectListAndApplicationDataListInTheSameOrder(
        detailsForAppsHostedInThisHost.sort((a, b) => b.apdex - a.apdex).slice(0, 25),
        app.getTopAppsByHost(randomHostName)
      )
      done()
    })

    it('Adding an app with an apdex value higher than the last app in the top apps list for that host will update the top apps list', done => {
      let randomHostName = getRandomElementFromArray(Object.keys(app._hostsByName))
      let old25thApp = app.getTopAppsByHost(randomHostName)[24]
      let apdexFor25thApp = old25thApp.getApdex()
      let appWithHighApdexDetails = {name: 'newAppInTopList', version: 1, apdex: apdexFor25thApp + 1}
      app.addAppToHosts(appWithHighApdexDetails, randomHostName)
      expect(app.getTopAppsByHost(randomHostName)).to.not.include(old25thApp)
      expect(app.getTopAppsByHost(randomHostName).map(x => x.getName() + x.getVersion())).to.include(appWithHighApdexDetails.name + appWithHighApdexDetails.version)
      done()
    })

    it('Adding an app with an apdex value lower than the last app in the top apps list for that host won`t change the top apps list', done => {
      let randomHostName = getRandomElementFromArray(Object.keys(app._hostsByName))
      let old25thApp = app.getTopAppsByHost(randomHostName)[24]
      let apdexFor25thApp = old25thApp.getApdex()
      let appWithLowApdexDetails = {name: 'newAppNotInTopList', version: 1, apdex: apdexFor25thApp - 1}
      app.addAppToHosts(appWithLowApdexDetails, randomHostName)
      expect(app.getTopAppsByHost(randomHostName)).to.include(old25thApp)
      expect(app.getTopAppsByHost(randomHostName).map(x => x.getName() + x.getVersion())).to.not.include(appWithLowApdexDetails.name + appWithLowApdexDetails.version)
      done()
    })

    it('Removing an app that belongs to the top apps list of a specific host, will remove the app from the top apps list and add to it an app with the next highest apdex', done => {
      let randomHostName = getRandomElementFromArray(Object.keys(app._hostsByName))
      let randomTopApp = getRandomElementFromArray(app.getTopAppsByHost(randomHostName))
      let nextApp = app._hostsByName[randomHostName].getAllHostedApps().sort((a, b) => b.getApdex() - a.getApdex())[25]
      app.removeAppFromHosts({
        name: randomTopApp.getName(),
        version: randomTopApp.getVersion()
      }, randomHostName)
      expect(app.getTopAppsByHost(randomHostName)).to.not.include(randomTopApp)
      expect(app.getTopAppsByHost(randomHostName).length).to.equal(25)
      expect(app.getTopAppsByHost(randomHostName)[24].getApdex()).to.equal(nextApp.getApdex())
      done()
    })

    it('Removing an app that doesn`t belong to the hosts` top apps list, doesn`t affect the top apps list', done => {
      let randomHostName = getRandomElementFromArray(Object.keys(app._hostsByName))
      let randomNotTopApp = getRandomElementFromArray(app._hostsByName[randomHostName].getAllHostedApps()
        .filter(x => !app.getTopAppsByHost(randomHostName).includes(x)))
      let previousTopAppsList = app.getTopAppsByHost(randomHostName)
      app.removeAppFromHosts({
        name: randomNotTopApp.getName(),
        version: randomNotTopApp.getVersion()
      }, randomHostName)
      expect(previousTopAppsList).to.have.members(app.getTopAppsByHost(randomHostName))
      done()
    })
  })
  describe('Adding apps to hosts', () => {
    it('Adding any app to a host that didn`t exist will create a new Host object for this host and setup the references correctly', done => {
      let newHostURL = 'new.host.url'
      let randomApplicationDataObject = getRandomElementFromArray(app._getAllApps())
      expect(app._hostsByName).to.not.have.property(newHostURL)
      app.addAppToHosts({
        name: randomApplicationDataObject.getName(),
        version: randomApplicationDataObject.getVersion()
      }, newHostURL)
      expect(app._hostsByName[newHostURL].getURL()).to.equal(newHostURL)

      expect(app._hostsByName[newHostURL].getAllHostedApps()).to.include(randomApplicationDataObject)
      expect(randomApplicationDataObject.getDeployments()).to.include(app._hostsByName[newHostURL])
      done()
    })

    it('Adding to any host an app whose name and version don`t match an existing one will create a new applicationData object and setup the references correctly', done => {
      let randomHostName = getRandomElementFromArray(Object.keys(app._hostsByName))
      let newAppDetails = {name: 'newApplicationDetails', version: 13}
      expect(app._appsByNameAndVersion).to.not.have.property(newAppDetails.name)
      app.addAppToHosts(newAppDetails, randomHostName)
      expect(app._appsByNameAndVersion[newAppDetails.name][newAppDetails.version].getName()).to.equal(newAppDetails.name)
      expect(app._appsByNameAndVersion[newAppDetails.name][newAppDetails.version].getVersion()).to.equal(newAppDetails.version)

      expect(app._hostsByName[randomHostName].getAllHostedApps()).to.include(app._appsByNameAndVersion[newAppDetails.name][newAppDetails.version])
      expect(app._appsByNameAndVersion[newAppDetails.name][newAppDetails.version].getDeployments()).to.include(app._hostsByName[randomHostName])
      done()
    })

    it('Adding an existing app to an existing host where it wasn`t hosted before, creates the references correctly', done => {
      let randomHostName = getRandomElementFromArray(Object.keys(app._hostsByName))
      let randomHostInstance = app._hostsByName[randomHostName]
      let randomApplicationDataObject = getRandomElementFromArray(
        app._getAllApps().filter(x => !x.getDeployments().includes(randomHostInstance)))

      expect(randomApplicationDataObject.getDeployments()).to.not.include(randomHostInstance)
      expect(randomHostInstance.getAllHostedApps()).to.not.include(randomApplicationDataObject)
      app.addAppToHosts({
        name: randomApplicationDataObject.getName(),
        version: randomApplicationDataObject.getVersion()
      }, randomHostName)
      expect(randomApplicationDataObject.getDeployments()).to.include(randomHostInstance)
      expect(randomHostInstance.getAllHostedApps()).to.include(randomApplicationDataObject)
      done()
    })

    it('Adding an existing app to a host where it was already hosted has no effect', done => {
      let randomApplicationDataObject = getRandomElementFromArray(app._getAllApps())
      let hostInstance = getRandomElementFromArray(randomApplicationDataObject.getDeployments())

      expect(randomApplicationDataObject.getDeployments()).to.include(hostInstance)
      expect(hostInstance.getAllHostedApps()).to.include(randomApplicationDataObject)
      app.addAppToHosts({
        name: randomApplicationDataObject.getName(),
        version: randomApplicationDataObject.getVersion()
      }, hostInstance.getURL())
      expect(randomApplicationDataObject.getDeployments()).to.include(hostInstance)
      expect(hostInstance.getAllHostedApps()).to.include(randomApplicationDataObject)
      done()
    })
  })
  describe('Removing apps from hosts', () => {
    it('Removing an app from a host where it wasn`t hosted has no effect', done => {
      let randomApplicationDataObject = getRandomElementFromArray(app._getAllApps())
      let hostNameWhereAppIsntHosted = getRandomElementFromArray(
        Object.keys(app._hostsByName).filter(hostName => !randomApplicationDataObject.getDeployments().includes(app._hostsByName[hostName])))
      let initialHostList = randomApplicationDataObject.getDeployments()
      expect(initialHostList).to.not.include(app._hostsByName[hostNameWhereAppIsntHosted])
      app.removeAppFromHosts({
        name: randomApplicationDataObject.getName(),
        version: randomApplicationDataObject.getVersion()
      }, hostNameWhereAppIsntHosted)
      expect(initialHostList).to.have.members(randomApplicationDataObject.getDeployments())
      done()
    })

    it('Removing an app whose name and version don`t match any existing app in the system has no effect on the host and doesn`t create an app object', done => {
      let randomHostName = getRandomElementFromArray(Object.keys(app._hostsByName))
      let randomHostInstance = app._hostsByName[randomHostName]
      let initialApplicationDataList = app._getAllApps()
      let initialApplicationDataInHostList = randomHostInstance.getAllHostedApps()
      let newAppName = 'appThatDoesnotExist'
      expect(app._appsByNameAndVersion[newAppName]).to.be.undefined
      app.removeAppFromHosts({
        name: newAppName,
        version: 1
      }, randomHostName)
      expect(initialApplicationDataList).to.have.members(app._getAllApps())
      expect(initialApplicationDataInHostList).to.have.members(randomHostInstance.getAllHostedApps())
      done()
    })

    it('Removing an existing app from a host where it was hosted updates the references correctly', done => {
      let randomApplicationDataObject = getRandomElementFromArray(app._getAllApps())
      let hostInstance = getRandomElementFromArray(randomApplicationDataObject.getDeployments())
      app.removeAppFromHosts({
        name: randomApplicationDataObject.getName(),
        version: randomApplicationDataObject.getVersion()
      }, hostInstance.getURL())
      expect(randomApplicationDataObject.getDeployments()).to.not.include(hostInstance)
      expect(hostInstance.getAllHostedApps()).to.not.include(randomApplicationDataObject)
      done()
    })
  })
})

/**
 * Returns a random element from an an array passed as a parameter
 * @param {X[]} arr - Array from which we want to extract a random element
 * @returns {X} random element from the array arr
 */
function getRandomElementFromArray (arr) {
  return arr[Math.floor(Math.random() * arr.length)]
}
