import { readFile } from 'fs'
const importer = path => new Promise((resolve, reject) => {
  readFile(path, 'utf8', (err, data) => {
    if (err) reject(err)
    else {
      resolve(JSON.parse(data))
    }
  })
})
export {importer}
