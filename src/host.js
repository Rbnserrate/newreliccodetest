import '../styles/host.scss'
import ApplicationData from './applicationData.js'
const TOP_APPS_LENGTH = 25
/**
 * Class representing a host in the system (url, hosted apps)
*/
class Host {
  /**
   * @param {String} hostName - URL where the host is available
   */
  constructor (hostName) {
    this._url = hostName
    /** Dictionary containing all applicationData objects hosted in this host, keyed by name and version
     * @member {Object.<String, Object.<String, ApplicationData>>}
     * @private */
    this._hostedAppsByNameAndVersion = {}

    /** List of top applications, sorted by apdex
     * @member {ApplicationData[]}
     * @private */
    this._topApps = []

    /** The apdex score of the last app in the top apps list
     * @member {Number}
     * @private */
    this._thresholdApdex = -1
  }
  /**
   * Simple getter for the _url property
   * @returns {String} The value of _url property
  */
  getURL () {
    return this._url
  }
  /**
   * Returns an array of all applicationData objects that are hosted in this host
   * @returns {ApplicationData[]} - All applicationData objects hosted in this host
   */
  getAllHostedApps () {
    return Object.keys(this._hostedAppsByNameAndVersion).reduce((allApps, currentName) =>
      allApps.concat(
        Object.keys(this._hostedAppsByNameAndVersion[currentName])
          .reduce((allAppsWithThisName, currentVersion) =>
            allAppsWithThisName.concat(this._hostedAppsByNameAndVersion[currentName][currentVersion])
            , []))
      , [])
  }
  /**
   * Registers an app as hosted in this host.
   * If the app is already hosted, nothing will change.
   * @param {ApplicationData} newApp - App to get registered in the host
   * @returns {void}
   */
  registerApp (newApp) {
    if (!this._hostedAppsByNameAndVersion[newApp.getName()]) this._hostedAppsByNameAndVersion[newApp.getName()] = {}
    if (!this._hostedAppsByNameAndVersion[newApp.getName()][newApp.getVersion()]) {
      this._hostedAppsByNameAndVersion[newApp.getName()][newApp.getVersion()] = newApp
      this.addToTopAppsIfNeeded(newApp)
    }
  }
  /**
   * Removes an app from this host (if it was hosted here).
   * @param {ApplicationData} appToRemove - App that will get deregistered from the host
   * @returns {void}
   */
  unregisterApp (appToRemove) {
    if (this._hostedAppsByNameAndVersion[appToRemove.getName()] &&
      this._hostedAppsByNameAndVersion[appToRemove.getName()][appToRemove.getVersion()]) {
      delete this._hostedAppsByNameAndVersion[appToRemove.getName()][appToRemove.getVersion()]
      this._refreshTopAppsIfNeeded(appToRemove)
    }
  }
  /**
   * Simple getter for the _topApps property
   * @returns {ApplicationData[]} The value of _topApps property
  */
  getTopApps () {
    return this._topApps
  }
  /**
   * If the new App should be in the top apps list, it adds it there
   * and makes sure the list stays with TOP_APPS_LENGTH apps or less
   * @param {ApplicationData} newApp - App we will be checking
   * @returns {void}
   * @private
   */
  addToTopAppsIfNeeded (newApp) {
    /** Only if we have less than TOP_APPS_LENGTH apps or the score is higher than
     * the score of the last top app, we add it to the top apps list */
    if (this._topApps.length < TOP_APPS_LENGTH || newApp.getApdex() > this._thresholdApdex) {
      let insertIndex = this._topApps.length
      // Find out in which position the new app fits
      this._topApps.some((topApp, index) => {
        if (topApp.getApdex() < newApp.getApdex()) {
          insertIndex = index
          return true
        }
      })
      // Insert it in that position
      this._topApps.splice(insertIndex, 0, newApp)
      if (this._topApps.length > TOP_APPS_LENGTH) this._topApps.pop()
      // Update top apps list threshold
      this._thresholdApdex = this._topApps[this._topApps.length - 1].getApdex()
    }
  }
  /**
  * Returns a DIV element that contains a functional visual representation of this host and
  * the top 5 applications deployed in it.
  * @returns {HTMLElement} Container that holds all other element that make up the visual representation of the host
  */
  render () {
    let wrapper = document.createElement('div')
    wrapper.setAttribute('class', 'host')
    wrapper.innerHTML = `
    <h2>${this._url}</h2>
    <ul class="app-list"></ul>
    `
    let appList = wrapper.querySelector('.app-list')
    this._topApps.slice(0, 5).forEach(topApp => {
      let li = document.createElement('li')
      li.appendChild(topApp.render())
      appList.appendChild(li)
    })
    return wrapper
  }
  /**
   * If appToRemove belongs to top apps list, we remove it and, when possible,
   * add another app to the top app list so that it stays with TOP_APPS_LENGTH members
   * @param {ApplicationData} appToRemove - App we will be checking
   * @returns {void}
   * @private
   */
  _refreshTopAppsIfNeeded (appToRemove) {
    // Find topApps index for the app to deregister
    let topAppsDeleteIndex = -1
    this._topApps.some((app, index) => {
      if (appToRemove === app) {
        topAppsDeleteIndex = index
        return true
      }
    })
    // If it belongs to top apps list, remove it
    if (topAppsDeleteIndex >= 0) {
      this._topApps.splice(topAppsDeleteIndex, 1)
      let allHostedApps = this.getAllHostedApps()
      // Now, if possible, we need to add another app to the top list so that it stays @ TOP_APPS_LENGTH
      if (allHostedApps.length >= TOP_APPS_LENGTH) {
        // Lets find the app with highest apdex that's not in the top apps list
        let highestRatedNotInTopApps = allHostedApps
          .filter(app => !this._isAppInTopAppsList(app))
          .reduce((highestForNow, currentApp) => {
            if (currentApp.getApdex() > highestForNow.getApdex()) {
              return currentApp
            } else {
              return highestForNow
            }
          }, new ApplicationData({apdex: -1}))
        this._topApps.push(highestRatedNotInTopApps)
      }
      // Update top apps list threshold
      this._thresholdApdex = this._topApps[this._topApps.length - 1].getApdex()
    }
  }
  /**
   * Returns whether or not appToCheck is part of the top apps list
   * @param {ApplicationData} appToCheck - App we will be checking
   * @returns {Boolean} - Whether or not appToCheck is part of the top apps list
   * @private
   */
  _isAppInTopAppsList (appToCheck) {
    if (appToCheck.getApdex() > this._thresholdApdex) {
      // If its apdex is higher than threshold, it's in the top apps list
      return true
    } else if (appToCheck.getApdex() === this._thresholdApdex) {
      /** If its equal to the threshold, we compare appToCheck against all apps
       * in the top apps list that have an apdex value equal to threhsoldApdex
       */
      for (let i = this._topApps.length - 1; i >= 0; i--) {
        if (this._topApps[i] === appToCheck) {
          return true
        } else if (this._topApps[i].getApdex() > this._thresholdApdex) {
          return false
        }
      }
      return false
    } else {
      return false
    }
  }
}

export default Host
