import ApplicationData from './applicationData.js'
import Host from './host.js'
import '../styles/reset.scss'
import '../styles/app.scss'
/**
 * ID of the user tho whom all the information held in this object belongs
 * @memberOf App
 * @type {String}
*/
const USERID = 'averylongemailaddress@companyname.com'
/** Class representing our application.
*/
class App {
  /**
   * Create instance of our application.
   */
  constructor () {
    /** Object keyed by app name and version that stores the values the different apps present in the system
     * @member {ApplicationData[]}
     * @private */
    this._appsByNameAndVersion = {}

    /** Dictionary containing all hosts existing in the system, keyed by host name
     * @member {Object.<String, Host>}
     * @private */
    this._hostsByName = {}
  }
  /**
   * Imports data into the application. Does some processing over the data supplied in order to
   * optimize the association between hosts and apps so that the other methods in the class can
   * perform more efficiently. Due to the nature of the methods we need to implement, we want to store the
   * information in a way that allows for direct access to all the apps hosted in a specific host, without
   * having to loop over the entire app list.
   *
   * The processing consists of the following:
   * For each app to be imported, we analyze its list of hosts and make sure there are instances of Host objects
   * created for them. Then we create an ApplicationData object that will store the app details and a list with references to
   * the hosts obtained in the previous step. Lastly, we establish the direct association between hosts and apps hosted in them by
   * we registering the newly created ApplicationData object with each of the Host objects obtained in the first step.
   *
   *  @param {Object[]} data - Array of plain objects containing information about the
   * applications to import into the system
   * @returns {void}
   */
  importApplicationData (data) {
    data.forEach(appData => this.addAppToHosts(appData, appData.host))
  }
  /**
   * Retrieves the 25 top apps hosted in the host with the name specified by hostName
   * @param {String} hostName - The name of the host whose top apps will be returned
   * @returns {ApplicationData[]} - The list of top apps in the host
   */
  getTopAppsByHost (hostName) {
    if (this._hostsByName[hostName]) {
      return this._hostsByName[hostName].getTopApps()
    } else {
      return []
    }
  }
  /**
   * Registers an application as being deployed in a certain host
   * If the app name and version don`t match an existing applicationData instance
   * a new one will be created. Similarly, if any string in "hostNames" doesn't
   * match an existing Host object, a new one will be created.
   * Then in the end we establish a bidirectional association between the applicationData
   * and the Host objects created.
   * @param {Object} appDetails - Plain object containing the details of the app we want to register in the host
   * @param {String | String[]} hostNames - The name(s) of the host(s) where we want to register the app as deployed
   * @returns {void}
   */
  addAppToHosts (appDetails, hostNames) {
    if (!(hostNames instanceof Array)) hostNames = [hostNames]

    let hosts = this._createOrFindHosts(hostNames)
    let applicationData = this._createOrFindApp(appDetails)
    hosts.forEach(host => {
      host.registerApp(applicationData)
      applicationData.registerDeployment(host)
    })
  }
  /**
   * Removes an application deployment from a certain host.
   * Specifically, will remove from the applicationData object that matches the name and version specified in appDetails
   * the reference to the Host objects that match the host names specified in hostNames
   * @param {Object} appDetails - Plain object containing the details of the app we want to remove from the host
   * @param {String | String[]} hostNames - The name(s) of the host(s) where we want to register the app as deployed
   * @returns {void}
   */
  removeAppFromHosts (appDetails, hostNames) {
    if (!(hostNames instanceof Array)) hostNames = [hostNames]
    hostNames.forEach(hostName => {
      if (this._hostsByName[hostName] &&
        this._appsByNameAndVersion[appDetails.name] &&
        this._appsByNameAndVersion[appDetails.name][appDetails.version]) {
        this._appsByNameAndVersion[appDetails.name][appDetails.version].removeDeployment(this._hostsByName[hostName])
        this._hostsByName[hostName].unregisterApp(this._appsByNameAndVersion[appDetails.name][appDetails.version])
      }
    })
  }
  /**
   * Returns a DIV element that contains a functional visual representation of the applications and hosts that are
   * in the system
   * @returns {HTMLElement} Container that holds all other element that make up the visual representation of the system
   */
  render () {
    let wrapper = document.createElement('div')
    wrapper.setAttribute('id', 'mainAppWrapper')
    wrapper.innerHTML = `
    <header>
      <h1><strong>Apps by Host</strong><span>for user ${USERID}</span></h1>
      <label>
        <input type="checkbox" id="layout-switch"/>
        <span>Show as <span id="layout-name">a list</span>
      </label>
    </header>
    <main>
      <ul id="host-list"></ul>
    </main>
    `
    let layoutName = wrapper.querySelector('#layout-name')
    wrapper.querySelector('#layout-switch').addEventListener('change', (e) => {
      if (e.currentTarget.checked) {
        layoutName.innerHTML = 'an awesome grid'
      } else {
        layoutName.innerHTML = 'a list'
      }
      wrapper.classList.toggle('gridLayout', e.currentTarget.checked)
    })
    let hostList = wrapper.querySelector('#host-list')
    Object.keys(this._hostsByName).forEach(key => {
      let li = document.createElement('li')
      li.appendChild(this._hostsByName[key].render())
      hostList.appendChild(li)
    })
    return wrapper
  }
  /**
   * Returns all the applicationData objects that are in the system
   * @returns {ApplicationData[]} -  All applicationData objects in the system
   * @private
   */
  _getAllApps () {
    return Object.keys(this._appsByNameAndVersion).reduce((allObjects, currentKey) =>
      allObjects.concat(Object.keys(this._appsByNameAndVersion[currentKey]).reduce((allObjectsForThisName, currentVersion) =>
        allObjectsForThisName.concat(this._appsByNameAndVersion[currentKey][currentVersion])
        , []))
      , [])
  }
  /**
   * Private function that returns the host instances that match the supplied host names
 * If the host instance doesn't exist it will be created.
 * @param {String[]} hostNames - Each member of this array is the name of the host to find/create
 * @returns {Host[]} Array of host instances
 * @private
 */
  _createOrFindHosts (hostNames) {
    return hostNames.map(hostName => {
      if (!this._hostsByName[hostName]) {
        this._hostsByName[hostName] = new Host(hostName)
      }
      return this._hostsByName[hostName]
    })
  }

  /**
   * Private function that returns the applicationData instance that matches the details supplied in
   * the appDetails parameter.
   * If the app doesn`t exist it will be created
   * @param {Object} appDetails - Details of the application whose instance we want to find / create
   * @returns {ApplicationData} ApplicationData instance for the supplied name and version
   * @private
   */
  _createOrFindApp (appDetails) {
    if (!this._appsByNameAndVersion[appDetails.name]) this._appsByNameAndVersion[appDetails.name] = {}
    if (!this._appsByNameAndVersion[appDetails.name][appDetails.version]) {
      this._appsByNameAndVersion[appDetails.name][appDetails.version] = new ApplicationData(appDetails)
    }
    return this._appsByNameAndVersion[appDetails.name][appDetails.version]
  }
}

export default App
