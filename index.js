import App from './src/app.js'
import IMPORT_DATA from './data/host-app-data.json'

let MainApp = new App()
MainApp.importApplicationData(IMPORT_DATA)
document.body.appendChild(MainApp.render())
 